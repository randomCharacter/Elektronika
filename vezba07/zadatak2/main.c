#include <avr/io.h>
#include <util/delay.h>

// Simboli A, B, C, d
const unsigned char symbol[] = { 0x0c, 0xa4, 0x27, 0xc4 };

int main() {
	unsigned char display;
	DDRD = 0xff; // PORT D je izlaz
	DDRB = 0x0f; // PB3 - PB0 su izlazi
	while (1) {
		for (display = 0; display < 4; display++) {
			// Ukljucuje tranzistor za odgovarajuci deo
			PORTB = ~(0x01 << (4 - display));
			// Ispisuje simbol na dati deo
			PORTD = symbol[display];
		}
	}
	return 0;
}
