# 1) LED diode
Napisati program koji koristi LED diode PLS7 ploce kao brojac koji se povecava svake sekunde

# 2) 7-segmentni displej
Napisati program koji zpisuje `ABCd` na 7-segmentnom displeju PLS7 ploce

# 3) Tasteri
Napisati program koji ispisuje `ABCd` na 7-segmentnom displeju PLS7 ploce ukoliko su odgovarajuci tasteri pritisnuti (`S0` - `S3`), odnosno `-` ukoliko nisu

# 4) Prekidaci
Napisati program koji ukljucuje LED diode (`LED0` - `LED7`) ukoliko je odgovarajuci prekidac (`P0` - `P7`) ukljucen