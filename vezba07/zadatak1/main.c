#include <avr/io.h>
#include <util/delay.h>
int main() {
	DDRD = 0xff;        // PORT D je izlaz
	DDRB |= 1 << 4;     // PB4 je izlaz
	PORTB &= ~(1 << 4); // PB4 = 0, tranzistor Q0 je upaljen
	int i = 0;
	while (1) {
		PORTD = ~i;      // Postavlja LED diode na invertovan (diode su upaljene na 0)
		_delay_ms(1000); // Ceka 1 sekundu
		i++;             // Povecava brojac za 1
	}
	return 0;
}
