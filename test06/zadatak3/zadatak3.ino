void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  Serial.println("Unesite izraz:\n");
  while(!Serial.available());
  delay(100);
  int c = Serial.available();
  String str = Serial.readString();

  int i1, i2;

  while (str.indexOf('*') != -1) {
    int i = str.indexOf('*');
    int j, k;
    for (j = i-1; j != 0; j--) {
      if (str[j] == '+' || str[j] == '-' || str[j] == '*' || str[j] == '/') {
        break;
      }
    }
    if (j) j++;
    for (k = i+1; k != str.length(); k++) {
      if (str[k] == '+' || str[k] == '-' || str[k] == '*' || str[k] == '/') {
        break;
      }
    }
    k--;
    String s1 = str.substring(j, i);
    String s2 = str.substring(i+1, k+1);
    int C = s1.toInt() * s2.toInt();
    String c = String(C);
    str.replace(s1 + "*" + s2, c);
  }

  while (str.indexOf('/') != -1) {
    int i = str.indexOf('/');
    int j, k;
    for (j = i-1; j != 0; j--) {
      if (str[j] == '+' || str[j] == '-' || str[j] == '*' || str[j] == '/') {
        break;
      }
    }
    if (j) j++;
    for (k = i+1; k != str.length(); k++) {
      if (str[k] == '+' || str[k] == '-' || str[k] == '*' || str[k] == '/') {
        break;
      }
    }
    k--;
    String s1 = str.substring(j, i);
    String s2 = str.substring(i+1, k+1);
    int C = s1.toInt() / s2.toInt();
    String c = String(C);
    str.replace(s1 + "/" + s2, c);
  }

  while (str.indexOf('+') != -1) {
    int i = str.indexOf('+');
    int j, k;
    for (j = i-1; j != 0; j--) {
      if (str[j] == '+' || str[j] == '-' || str[j] == '*' || str[j] == '/') {
        break;
      }
    }
    if (j) j++;
    for (k = i+1; k != str.length(); k++) {
      if (str[k] == '+' || str[k] == '-' || str[k] == '*' || str[k] == '/') {
        break;
      }
    }
    k--;
    String s1 = str.substring(j, i);
    String s2 = str.substring(i+1, k+1);
    int C = s1.toInt() + s2.toInt();
    String c = String(C);
    str.replace(s1 + "+" + s2, c);
  }

  while (str.indexOf('-') != -1) {
    int i = str.indexOf('-');
    int j, k;
    for (j = i-1; j != 0; j--) {
      if (str[j] == '+' || str[j] == '-' || str[j] == '*' || str[j] == '/') {
        break;
      }
    }
    if (j) j++;
    for (k = i+1; k != str.length(); k++) {
      if (str[k] == '+' || str[k] == '-' || str[k] == '*' || str[k] == '/') {
        break;
      }
    }
    k--;
    String s1 = str.substring(j, i);
    String s2 = str.substring(i+1, k+1);
    int C = s1.toInt() - s2.toInt();
    String c = String(C);
    str.replace(s1 + "-" + s2, c);
    
  }

Serial.println(str);
  
}
