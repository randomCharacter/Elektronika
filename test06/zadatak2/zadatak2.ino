void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  Serial.println("Unesite izraz:\n");
  while(!Serial.available());
  double a = Serial.parseFloat();
  char op1;
  Serial.readBytes(&op1, 1);
  double b = Serial.parseFloat();
  char op2;
  Serial.readBytes(&op2, 1);
  double c = Serial.parseFloat();

  double r;
  
  switch (op1) {
    case '+':
      switch (op2) {
        case '+':
          r = a + b + c;
          break;
        case '-':
          r = a + b - c;
          break;
        case '*':
          r = a + b * c;
          break;
        case '/':
          r = a + b / c;
          break;
      }
      break;
    case '-':
     switch (op2) {
        case '+':
          r = a - b + c;
          break;
        case '-':
          r = a - b - c;
          break;
        case '*':
          r = a - b * c;
          break;
        case '/':
          r = a - b / c;
          break;
      }
      break;
    case '*':
      switch (op2) {
        case '+':
          r = a * b + c;
          break;
        case '-':
          r = a * b - c;
          break;
        case '*':
          r = a * b * c;
          break;
        case '/':
          r = a * b / c;
          break;
      }
      break;
    case '/':
      switch (op2) {
        case '+':
          r = a / b + c;
          break;
        case '-':
          r = a / b - c;
          break;
        case '*':
          r = a / b * c;
          break;
        case '/':
          r = a / b / c;
          break;
      }
      break;
  }

  Serial.print(a);
  Serial.print(op1);
  Serial.print(b);
  Serial.print(op2);
  Serial.print(c);
  Serial.print("=");
  Serial.println(r);

}
