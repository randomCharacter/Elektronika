void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  Serial.println("Unesite izraz:\n");
  while(!Serial.available());
  double a = Serial.parseFloat();
  char op;
  Serial.readBytes(&op, 1);
  double b = Serial.parseFloat();
  double c;
  
  switch (op) {
    case '+':
      c = a + b;
      break;
    case '-':
      c = a - b;
      break;
    case '*':
      c = a * b;
      break;
    case '/':
      c = a / b;
      break;
  }

  Serial.print(a);
  Serial.print(op);
  Serial.print(b);
  Serial.print("=");
  Serial.println(c);

}
