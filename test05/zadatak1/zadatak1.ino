void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  static double Vbe_on = 0.7, Vces = 0.2;
  double Vbb, Vcc, Rb, Rc, beta;
  Serial.println("Vbb=");
  while(!Serial.available());
  Vbb = Serial.parseFloat();

  Serial.println("Vcc=");
  while(!Serial.available());
  Vcc = Serial.parseFloat();
  
  Serial.println("Rb=");
  while(!Serial.available());
  Rb = Serial.parseFloat();

  
  Serial.println("Rc=");
  while(!Serial.available());
  Rc = Serial.parseFloat();
  
  Serial.println("beta=");
  while(!Serial.available());
  beta = Serial.parseFloat();

  double Vbe = Vbb - Vbe_on;
  double Vce = Vcc - Vces;
  double ic = Vce / Rb;
  double ib = Vbe / Rc;
  if (Vbb < Vbe_on) {
    Serial.println("STANJE: Zakocenje");
    Serial.print("Vbe = ");
    Serial.println(Vbb);
    Serial.println("ib = 0");
    Serial.println("ic = 0");
  } else if (ic < beta * ib) {
    Serial.println("STANJE: ZASICENJE");
    Serial.print("Vbe = ");
    Serial.println(Vbe);
    Serial.print("ib =");
    Serial.println(ib);
    Serial.print("ic =");
    Serial.println(ic);
  } else {
    Serial.println("STANJE: AKTIVNO");
    Serial.print("Vbe = ");
    Serial.println(Vbe);
    Serial.print("ib =");
    Serial.println(ib);
    Serial.print("ic =");
    Serial.println(ic);
  }
}
