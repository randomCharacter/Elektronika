#include <avr/io.h>
#include <avr/interrupt.h>

const unsigned char dzo[] = {
		0xc5, 0x26, 0xc4, 0xdd, 0xa6, 0x26, 0xff, 0x8c, 0x5, 0xc4, 0xff, 0xc4, 0x46, 0x5, 0xc, 0xff, 0xff, 0xff, 0xff, 0x00
   };

const unsigned char sljiva[] = {
		0xc, 0x2e, 0xee, 0xdd, 0x27, 0x8c, 0xc, 0xff, 0x34, 0xa7, 0xc5, 0xdd, 0xe5, 0xc, 0xff, 0xfe, 0xff, 0xec, 0x26, 0xff, 0xe, 0xc, 0xc4, 0xc, 0xff, 0xff, 0xff, 0xff, 0x00};

unsigned char DISP_BUFFER[4] = {
		0xff, 0xff, 0xff, 0xff
   };

unsigned long millis = 0;
unsigned char disp = 3;
int j = 0;
unsigned char a = 15;

int smer = 0;
int reklama = 0;

ISR(TIMER0_COMPA_vect)
{
	if (++disp > 3)
		disp = 0;
	PORTB = ~(1 << (3-disp));
	PORTD = DISP_BUFFER[disp];
	millis++;
}

ISR(PCINT1_vect)
{
	switch ((~PINC) & 0xf)
	{
		case 0x01: //pritisnut levi taster
		{
			smer = 0;
			break;
		}
		case 0x02: //pritisnut donji taster

			reklama = 1;
			j = 0;
			break;
		case 0x04: //pritisnut desni taster
		{
			smer = 1;
			break;
		}
		case 0x08:	//pritisnut gornji taster
		{
			reklama = 0;
			j = 0;
			break;
		}

	}
}

int main() {
	unsigned long t0;
	unsigned char i;

	//inicijalizacija portova:
	DDRB = 0x0f; //PB3-PB0 -> izlazi
	DDRC = 0x00; //port C -> ulaz
	DDRD = 0xff; //port D -> izlaz
	//inicijalizacija tajmera 0:
	PCICR = (1 << PCIE1); //dozvola prekida usled promene stanja; isto sto i PCICR = 0x02;
	PCMSK1 = 0xf; //pina PCINT10, ili pina PCINT8
	TCCR0A = 0x02; //tajmer 0: CTC mod
	TCCR0B = 0x03; //tajmer 0: fclk = fosc/64
	OCR0A = 249; //perioda tajmera 0: 250 Tclk (OCR0A + 1 = 250)
	TIMSK0 = 0x02; //dozvola prekida tajmera 0
	//usled dostizanja vrednosti registra OCR0A
	sei(); //I = 1 (dozvola prekida)

	while(1) {
		t0 = millis;

		while ((millis - t0) < 500); //pauza 500ms
		if(reklama == 0) {
			for (i = 0; i < 4; i++) {
				DISP_BUFFER[i] = dzo[j+i];
			}
			if (!smer) {
				j++;
			}  else {
				j--;
			}
			if(j == 15) {
				j = 0;
			}
			if (j == -1) {
				j = 14;
			}
		} else if(reklama == 1) {
			for (i = 0; i < 4; i++) {
				DISP_BUFFER[i] = sljiva[j+i];
			}
			if (!smer) {
				j++;
			} else {
				j--;
			}
			if(j == 25) {
				j = 0;
			}
			if (j == -1) {
				j = 14;
			}
		}

	}
	return 0;
}
