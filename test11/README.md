# test11
Napisati program koji ispisuje svetlecu reklamu `"Jedite kod Dzoa"` i `"Africka sljiva - ne pada"`. Pritiskom na taster `UP` bira se prva reklama, dok se pritiskom na taster `DOWN` bira druga reklama. Smer kretanja reklame bira se na tasterima `LEFT` i `RIGHT`. Reklama se pomeri za jedan karakter u odgovarajucu stranu na svakih `500ms`.
