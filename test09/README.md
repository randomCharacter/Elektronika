# test09
## 1)
Napisati program koji u zavisnosti od polozaja prekidaca `SW1` i `SW2` na 7segmentnom displeju ispisuje `FIAt`, `rEnO`, `OPEL` ili `FOrd`.
## 2)
Prosiriti prethodni program tako da ispisana poruka u zavisnosti od polozaja tastera `SW3` i `SW4` naizmenicno pali i gasi izabranu poruku na svakih `1000ms`, `500ms`, `250ms` odnosno stoji stalno upaljena ukoliko su oba tastera iskljucena.
