#include <avr/io.h>
#include <util/delay.h>

#define SCL_HI (PORTC |= (1<<5))
#define SCL_LO (PORTC &= ~(1<<5))
#define SDA (PINC & (1 << 4))
#define SHLD_HI (PORTB |= (1<<5))
#define SHLD_LO (PORTB &= ~(1<<5))

const unsigned char fiat[] = { 0x2E, 0xAF, 0x0C, 0xa6 };
const unsigned char reno[] = { 0xEE, 0x26, 0xEC, 0x05 };
const unsigned char opel[] = { 0x05, 0x0E, 0x26, 0xA7 };
const unsigned char ford[] = { 0x2E, 0x05, 0xEE, 0xc4 };
const unsigned char nita[] = { 0xFF, 0xff, 0xff, 0xff };
const unsigned char mask1 = 0x03;
const unsigned char mask2 = 0x0c;

void ispis_7SEG (unsigned char karakter, unsigned char pozicija) {

	PORTB = ~(0x01 << (4-pozicija));
	PORTD = karakter;
	_delay_ms(2);
}

unsigned char ocitaj_prekidace() {

	unsigned char i, tmp = 0, mask = 0x80;

	SHLD_HI;
	SHLD_LO;
	SHLD_HI;

	for (i=0; i<8; i++) {
		SCL_LO;
		SCL_HI;
		if (SDA)
			tmp |= mask;
			mask >>= 1;
	}

	return tmp;
}


int main(void) {
	DDRD = 0xff;
	DDRB = 0x3f;
	DDRC = 0x20;
	int count;
	while (1) {

		unsigned char a = ocitaj_prekidace();

		int i, n;

		if(!count) {
			if ((a & mask1) == 0x03) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(fiat[i], i+1);
				}
			} else if((a & mask1) == 0x02) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(reno[i], i+1);
				}
			} else if((a & mask1) == 0x01) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(opel[i], i+1);
				}
			} else if((a & mask1) == 0x00) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(ford[i], i+1);
				}
			}
		}

		for (n = 0; n < count; n++) {
			if ((a & mask1) == 0x03) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(fiat[i], i+1);
				}
			} else if((a & mask1) == 0x02) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(reno[i], i+1);
				}
			} else if((a & mask1) == 0x01) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(opel[i], i+1);
				}
			} else if((a & mask1) == 0x00) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(ford[i], i+1);
				}
			}
		}
		if (count) {
			for (n = 0; n < count; n++) {
				for (i = 0; i < 4; i++) {
					ispis_7SEG(nita[i], i+1);
				}
			}
		}


		if (((a & mask2) >> 2) == 0x01) {
			count = 1000;
		} else if (((a & mask2) >> 2) == 0x02) {
			count = 500;
		} else if (((a & mask2) >> 2) == 0x03) {
			count = 250;
		} else {
			count = 0;
		}
	}

	return 0;
}
