# Zadatak 2
Napisati program koji omogućava korisniku unos broja poena osvojenih na ispitu 
i na osnovu unete vrednosti izračunava i prikazuje ocenu.

```
IZLAZ: 	Unesite broj poena:
ULAZ: 	83
IZLAZ: 	Uneli ste 83 poena => dobili ste ocenu 8.
```
