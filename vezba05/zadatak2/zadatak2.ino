void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  int poeni, ocena;

  Serial.println("Unesite broj poena:");

  // Ceka da se pojavi broj
  while(!Serial.available());
  // Ucitava broj
  poeni = Serial.parseInt();
  // Ukoliko uneti rezultat nije validan ponavlja se sve
  if (poeni < 0 || poeni > 100) {
    return;
  }

  // Racunanje ocene
  ocena = (poeni + 5) / 10;
  if (ocena < 5) {
    ocena = 5;
  }

  // Ispis
  Serial.print("Uneli ste ");
  Serial.print(poeni);
  Serial.print(" poena => dobili ste ocenu ");
  Serial.print(ocena);
  Serial.print(".");

  // Kraj
  while(1);

}
