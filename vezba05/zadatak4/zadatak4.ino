int palindrom(String string) {
  int n = string.length();
  for (int i = 0; i < n/2; i++) {
    // Izlazi ako naidje na razlicite karaktere na suprotnim stranama
    if (string.charAt(i) != string.charAt(n-(i+1))) {
      return 0;
    }
  }
  // Nije naisao na suprotne karaktere
  return 1;
}

void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  String string;

  Serial.println("Unesite string:");

  //Ucitavanje stringa
  while(!Serial.available());
  string = Serial.readString();

  Serial.print("\"");
  Serial.print(string);
  if (palindrom(string)) {
    Serial.println("\" jeste palindrom");
  } else {
    Serial.println("\" nije palindrom");
  }
}
