# Zadatak 4
Napisati program koji omogućava korisniku unos stringa, a zatim proverava
da li je uneti string palindrom.

```
IZLAZ: 	Unesite string:
ULAZ: 	papak
IZLAZ: 	“papak” nije palindrom.
IZLAZ: 	Unesite string:
ULAZ: 	kapak
IZLAZ: 	“kapak” jeste palindrom.
```
