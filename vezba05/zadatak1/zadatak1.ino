void setup() {
  // Otvaranje serijskog porta
  Serial.begin(9600);
  // Ceka se serial monitor
  delay(1000);
}

void loop() {
  static int A = 0;
  static int B = 1;

  int C = A + B;
  A = B;
  B = C;

  // Kraj zbog prekoracenja
  if (C < A) {
    Serial.end();
    while(1); //kraj
  }

  Serial.println(C);

  delay(1000);
}
