# Zadatak 1
Napisati program koji preko serijskog porta ispisuje Fibonačijeve brojeve, 
svaki u posebnom redu. Između svaka 2 uzastopna ispisa napraviti pauzu u trajanju od 1s. 
Ispis treba zaustaviti neposredno pre prekoračenja opsega vrednosti.

```
IZLAZ: 	1
1
2
3
5
8
...
```
