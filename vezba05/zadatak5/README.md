# Zadatak 5
Korisnik zadaje koeficijente a, b i c u kvadratnoj jednacini a*x^2 + b*x + c = 0.
Napisati program koji ispisuje resenja x1 i x2 (u kompleksnoj formi, ako je 
diskriminanta negativna).

```
IZLAZ: Unesite koeficijente a, b i c:
ULAZ: 1.0 2.0 3.0
IZLAZ: x1 = -1.0 + i * 1.41, x2 = -1.0 - i * 1.41
```
