void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  double a, b, c;

  Serial.println("Unesite koeficijente a, b i c:");
  // Uciravanje
  while(!Serial.available());
  a = Serial.parseFloat();
  while(!Serial.available());
  b = Serial.parseFloat();
  while(!Serial.available());
  c = Serial.parseFloat();

  // Racunanje diskriminante
  float d = b * b - 4 * a * c;

  if (a == 0) {
    Serial.print("x = ");
    Serial.println(-b/c);
  } else if (d == 0) {
    Serial.print("x = ");
    Serial.println(-b/(2*a));
  } else if (d > 0) {
    Serial.print("x1 = ");
    Serial.print((-b + sqrt(d))/(2*a));
    Serial.print(", x2 = ");
    Serial.println((-b - sqrt(d))/(2*a));
  } else {
    Serial.print("x1 = ");
    Serial.print(-b/(2*a));
    Serial.print(" + i * ");
    Serial.print(sqrt(-d)/(2*a));
    Serial.print(", x2 = ");
    Serial.print(-b/(2*a));
    Serial.print(" - i * ");
    Serial.print(sqrt(-d)/(2*a));
  }
}
