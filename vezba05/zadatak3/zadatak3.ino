void setup() {
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  int t1, t2, v1, v2, v3;

  // Ucitavanje bodova po delovima
  Serial.println("Unesite broj poena iz teorije (1. deo) :");
  while(!Serial.available());
  t1 = Serial.parseInt();
  if (t1 < 0 || t1 > 25) {
    return;
  }
  
  Serial.println("Unesite broj poena iz teorije (2. deo) :");
  while(!Serial.available());
  t2 = Serial.parseInt();
  if (t2 < 0 || t2 > 25) {
    return;
  }

  Serial.println("Unesite broj poena iz zadataka (1. deo) :");
  while(!Serial.available());
  v1 = Serial.parseInt();
  if (v1 < 0 || v1 > 20) {
    return;
  }

  Serial.println("Unesite broj poena iz zadataka (2. deo) :");
  while(!Serial.available());
  v2 = Serial.parseInt();
  if (v2 < 0 || v2 > 20) {
    return;
  }

  Serial.println("Unesite broj poena iz zadataka (3. deo) :");
  while(!Serial.available());
  v3 = Serial.parseInt();
  if (v3 < 0 || v3 > 20) {
    return;
  }

  // Racunaje ocene
  int poeni = t1 + t2 + v1 + v2 + v3;

  int ocena = (poeni + 5) / 10;

  if (ocena < 5) {
    ocena = 5;
  }

  // Ispis
  Serial.print("Uneli ste ");
  Serial.print(poeni);
  Serial.print(" poena => dobili ste ocenu ");
  Serial.print(ocena);
  Serial.print(".");

  // Kraj
  while(1);
}
