# Zadatak 3
Modifikovati program iz prethodnog zadatka tako da omogući unos poena osvojenih 
na kolokvijumima  i na vežbama (T1 max. 25, T2 max.25, V1 max. 20, V2 max. 20,
V3 max. 20), sabira poene i na osnovu toga ispisuje ocenu. Pri svakom od unosa
treba proveriti da li je unesen korektan broj poena, ako nije unos se prekida
i vraća se na početak.

```
IZLAZ: 	Unesite broj poena iz teorije (1. deo) :
ULAZ: 	18
IZLAZ: 	Unesite broj poena iz teorije (2. deo) :
ULAZ: 	23
IZLAZ: 	Unesite broj poena iz zadataka (1. deo) :
ULAZ: 	13
IZLAZ: 	Unesite broj poena iz zadataka (2. deo) :
ULAZ: 	15
IZLAZ: 	Unesite broj poena iz zadataka (3. deo) :
ULAZ: 	7
IZLAZ: 	Osvojili ste ukupno 76 poena => dobili ste ocenu 8.
```
