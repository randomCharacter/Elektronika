# 1)
Napisati program koji preko serijskog porta ispisuje Fibonačijeve brojeve, 
svaki u posebnom redu. Između svaka 2 uzastopna ispisa napraviti pauzu u trajanju od 1s. 
Ispis treba zaustaviti neposredno pre prekoračenja opsega vrednosti.

```
IZLAZ: 	1
1
2
3
5
8
...
```


# 2)
Napisati program koji omogućava korisniku unos broja poena osvojenih na ispitu 
i na osnovu unete vrednosti izračunava i prikazuje ocenu.

```
IZLAZ: 	Unesite broj poena:
ULAZ: 	83
IZLAZ: 	Uneli ste 83 poena => dobili ste ocenu 8.
```

# 3)
Modifikovati program iz prethodnog zadatka tako da omogući unos poena osvojenih 
na kolokvijumima  i na vežbama (T1 max. 25, T2 max.25, V1 max. 20, V2 max. 20,
V3 max. 20), sabira poene i na osnovu toga ispisuje ocenu. Pri svakom od unosa
treba proveriti da li je unesen korektan broj poena, ako nije unos se prekida
i vraća se na početak.

```
IZLAZ: 	Unesite broj poena iz teorije (1. deo) :
ULAZ: 	18
IZLAZ: 	Unesite broj poena iz teorije (2. deo) :
ULAZ: 	23
IZLAZ: 	Unesite broj poena iz zadataka (1. deo) :
ULAZ: 	13
IZLAZ: 	Unesite broj poena iz zadataka (2. deo) :
ULAZ: 	15
IZLAZ: 	Unesite broj poena iz zadataka (3. deo) :
ULAZ: 	7
IZLAZ: 	Osvojili ste ukupno 76 poena => dobili ste ocenu 8.
```

# 4)
Napisati program koji omogućava korisniku unos stringa, a zatim proverava
da li je uneti string palindrom.

```
IZLAZ: 	Unesite string:
ULAZ: 	papak
IZLAZ: 	“papak” nije palindrom.
IZLAZ: 	Unesite string:
ULAZ: 	kapak
IZLAZ: 	“kapak” jeste palindrom.
```

# 5)
Korisnik zadaje koeficijente a, b i c u kvadratnoj jednacini a*x^2 + b*x + c = 0.
Napisati program koji ispisuje resenja x1 i x2 (u kompleksnoj formi, ako je 
diskriminanta negativna).

```
IZLAZ: Unesite koeficijente a, b i c:
ULAZ: 1.0 2.0 3.0
IZLAZ: x1 = -1.0 + i * 1.41, x2 = -1.0 - i * 1.41
```
