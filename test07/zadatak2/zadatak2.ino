#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

#define NONE   0
#define DOWN   1
#define LEFT   2
#define RIGHT  3
#define SELECT 4
#define UP     5

String reci[] = { "rec01", "rec02", "rec03", "rec04", "rec05", "rec06", "rec07", "rec08", "rec09", "rec10", "rec11", "rec12", "rec13", "rec14", "rec15", "rec16", "rec17", "rec18", "rec19", "rec20"};
int zivota, nadjenih;
String rec, otkrivena;

String abc = "abcdefghijklmnopqrstuvwxyz0123456789";

int getTaster() {
  int t = analogRead(0);
  if (t < 90) {
    return RIGHT;
  } else if (t < 240) {
    return UP;
  } else if (t < 350) {
    return DOWN;
  } else if (t < 600) {
    return LEFT;
  } else if (t < 1000) {
    return SELECT;
  }
  return NONE;
}

void setup() {
  randomSeed(analogRead(1));
  lcd.begin(16, 2);
  Serial.begin(9600);
  delay(100);
  rec = reci[random(20)];
  otkrivena = "_____";
  zivota = 10;
  nadjenih = 0;
}

void loop() {
  static int i = 0;
  char c = abc[i];
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(otkrivena);
  lcd.setCursor(0, 1);
  lcd.print(zivota);
  lcd.setCursor(15, 1);
  lcd.print(c);

  int selektovan = 0;
  while (!selektovan) {
    int t = getTaster();
    if (t == UP) {
      i = (i + 1) % abc.length();
      delay(100);
      return;
    } else if (t == DOWN) {
      i = (i - 1) % abc.length();
      delay(100);
      return;
    } else if (t == SELECT) {
      selektovan = 1;
      delay(250);
    }
  }
  
  int nasao = 0;
  for (int i = 0; i < rec.length(); i++) {
    if (rec[i] == c && otkrivena[i] == '_') {
      nasao = true;
      nadjenih++;
      otkrivena[i] = c;
    }
  }
  if (!nasao) {
    zivota--;
  }

  if (nadjenih == rec.length()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("preziveo :)");
    while(1);
  }

  if (zivota <= 0) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("obesen :(");
    while(1);
  }
}
