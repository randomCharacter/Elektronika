# test07
## 1)
Napisati program koji pogadja jednu od nasumicno izabranih 20 reci duzine 5 karaktera. Na samom pocetku igrac ima 10 zivota i preko serijskog porta unosi slovo. Na LCD displeju nalazi se `_____`, i svaki put kada igrac unese dobro slovo, karakter `_` na svakoj poziciji na kojoj se dato slovo nalaze se zamenjuje datim slovom, dok ukoliko promasi broj zivota se dekrementira. Ukoliko igrac ostane bez zivota, izgubio je. Ukoliko igrac uspe da pogodi rec, pobedio je. Na pocetku igrac ima 10 zivota.
## 2)
Dopuniti prethodni program tako da se selekcija reci vrsi direktno na LCD displeju, tako sto se tasterima `UP` i `DOWN` menjaju slova, a tasterom `SELECT` bira slovo.
