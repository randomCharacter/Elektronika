#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

String reci[] = { "rec01", "rec02", "rec03", "rec04", "rec05", "rec06", "rec07", "rec08", "rec09", "rec10", "rec11", "rec12", "rec13", "rec14", "rec15", "rec16", "rec17", "rec18", "rec19", "rec20"};
int zivota, nadjenih;
String rec, otkrivena;

void setup() {
  randomSeed(analogRead(1));
  lcd.begin(16, 2);
  Serial.begin(9600);
  delay(100);
  rec = reci[random(20)];
  otkrivena = "_____";
  zivota = 10;
  nadjenih = 0;
}

void loop() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(otkrivena);
  lcd.setCursor(0, 1);
  lcd.print(zivota);
  char c;

  while (!Serial.available());
  Serial.readBytes(&c, 1);
  int nasao = 0;
  for (int i = 0; i < rec.length(); i++) {
    if (rec[i] == c) {
      nasao = true;
      nadjenih++;
      otkrivena[i] = c;
    }
  }
  if (!nasao) {
    zivota--;
  }

  if (nadjenih == rec.length()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("preziveo :)");
    while(1);
  }

  if (zivota <= 0) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("obesen :(");
    while(1);
  }
}
