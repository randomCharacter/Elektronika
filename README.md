# Elektronika

Rešenja zadataka sa vežbi predmeta "Elektronika" koje su rađene 2017. godine na Fakultetu Tehničkih nauka u Novom Sadu. 

Ostatak materijala, kao i originalni tekstovi zadataka mogu se pronaći na [sajtu predmeta](http://www.elektronika.ftn.uns.ac.rs/index.php?option=com_content&task=category&sectionid=4&id=13&Itemid=54)
