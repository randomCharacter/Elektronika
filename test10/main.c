#include <avr/io.h>
#include <util/delay.h>

#include <avr/io.h>
#include <util/delay.h>

unsigned char rot(unsigned char x, int n, int smer) {
	if (smer) {
		unsigned char temp = x << (8 - n);
		x = x >> n;
		return (x | temp);
	} else {
		unsigned char temp = x >> (8 - n);
		    x = x << n;
		    return (x | temp);
	}
}


unsigned char led=0x0F;

int main(void) {
	DDRD=0xff;
	DDRB |= 1<<4;
	PORTB &=~(1<<4);

	int pravac=1;
	int i = 0;
	unsigned char tasteri;
	while(1){

		tasteri = PINC & 0x0f;
			if (!(tasteri & 0x01)) {
				pravac=0;
			} else if (!(tasteri & 0x04)) {
				pravac=1;
			} else if (!(tasteri & 0x02)) {
					if (led < 0x7f)
						led = (led << 1) | 0x1;
			} else if (!(tasteri & 0x08)) {
					if (led > 0x01)
						led = led >> 1;
			}


			_delay_ms(250);
			PORTD=rot(led, i, pravac);

			i = (i + 1) % 8;

	}
	return 0;
}
