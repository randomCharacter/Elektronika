# test10
Napisati program koji vrsi rotaciju `LED` dioda na ploci. Pritiskom na taster `LEFT` diode rotiraju s desna na levo, pritiskom na taster `RIGHT` diode rotiraju sa leva na desno. Pritiskom na taster `UP` broj dioda koje svetle se povecava (max 7), pritiskom na taster `DOWN` broj dioda koje svetle se smanjuje (min 1).
