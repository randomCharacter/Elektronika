# test12
Napisati program koji na 7segmentnom displeju prikazuje vreme u minutima i sekundima. Ovo vreme treba da se menja u realnom vremenu, a moguce ga je i rucno podesiti tasterima `UP` i `DOWN` za sate, odnosno `LEFT` i `RIGHT` za minute. Decimalna tacka koja odvaja minute od sata treba da se pali i gasi na svakih `500ms`.
