#include <avr/io.h>
#include <avr/interrupt.h>

const unsigned char simboli[] = {
		0x5, 0xdd, 0x46, 0x54, 0x9c, 0x34, 0x24, 0x5d, 0x04, 0x14
}; //look-up tabela sa simbolima
unsigned char DISP_BAFER[4] = {
		0xfe, 0xfe, 0xfe, 0xfe
}; //bafer displeja
unsigned long millis = 0;
unsigned char disp = 3;
int ukljuceno = 0;
int sati = 0;
int minuti = 0;
int sekunde = 0;
int flag = 0;
unsigned char tacka = 0xFF;
int prethodnoStanje;
int prethodnoStanje2;

ISR(TIMER0_COMPA_vect)
{
	//prekid tajmera 0 usled dostizanja vrednosti registra OCR0A
	if (++disp > 3)
		disp = 0;
	PORTB = ~(1 << (3-disp)); //ukljucenje tranzistora
	if (disp == 1) {
		PORTD = DISP_BAFER[disp] & tacka;
	} else {
		PORTD = DISP_BAFER[disp]; //ispis na trenutno aktivan displej
	}
	millis++; //sistemsko vreme
	if (millis == 500) {
		tacka ^= 0x04;
	}
	if (millis >= 1000) {
		millis = 0;
		if(++sekunde >= 60) {
			sekunde = 0;
			if (++minuti >= 60) {
				minuti = 0;
				if (sati >= 24) {
					sati = 0;
				}
			}
		}
	}
}

ISR(PCINT1_vect)
{
	//prekid usled promene stanja pina PCINT10 ili pina PCINT8
	switch ((~PINC) & 0xf)
	{
		case 0x01: //levo
		{			
			if(--minuti < 0)
			{
				minuti = 59;
			}
			break;
		}
		case 0x02: //dole
		{			
			if(--sati < 0)
			{
				sati = 23;
			}
			break;
		}
		case 0x04: //desno
		{
			if(++minuti >= 60)
			{
				minuti = 0;
			}
			break;
		}
		case 0x08:
		{
			//gore
			if(++sati >= 24)
			{
				sati = 0;
			}
			break;
		}

	}
	DISP_BAFER[0] = simboli[(sati / 10)];
	DISP_BAFER[1] = simboli[(sati % 10)];
	DISP_BAFER[2] = simboli[(minuti / 10)];
	DISP_BAFER[3] = simboli[(minuti % 10)];
}

int main() {
	//inicijalizacija portova:
	DDRB = 0x0f; //PB3-PB0 -> izlazi
	DDRC = 0x00; //port C -> ulaz
	DDRD = 0xff; //port D -> izlaz
	//inicijalizacija tajmera 0:
	PCICR = (1 << PCIE1); //dozvola prekida usled promene stanja; isto sto i PCICR = 0x02;
	PCMSK1 = 0xf; //pina PCINT10, ili pina PCINT8
	TCCR0A = 0x02; //tajmer 0: CTC mod
	TCCR0B = 0x03; //tajmer 0: fclk = fosc/64
	OCR0A = 249; //perioda tajmera 0: 250 Tclk (OCR0A + 1 = 250)
	TIMSK0 = 0x02; //dozvola prekida tajmera 0
	//usled dostizanja vrednosti registra OCR0A
	sei(); //I = 1 (dozvola prekida)
	int i;
	for (i = 0; i < 4; i++) {
		DISP_BAFER[i] = simboli[0];
	}

	while(1);

	return 0;
}
