# test08
## 1)
Igrac je predstavljen proizvoljnim karakterom i pocenje na koordinatama `(0,0)`. Mine su predstavljene karakterom `*`, ima ih osam, i generisu se na neparnim pozicijama ekrana. Na ostalim, praznim delovima LCD ekrana generisu se 3 zlatnika i izlaz. Igrac koristeci tastere `UP`, `DOWN`, `LEFT` i `RIGHT` treba da pokupi 3 zlatnika i ode na izlaz, a da pritom ne pokupi minu. Igra se zavrsava kada igrac pokupi minu (izgubio) ili kada pokupi 3 zlatnika i ode na izlaz (pobedio).
