#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

#define NONE   0
#define DOWN   1
#define LEFT   2
#define RIGHT  3
#define SELECT 4
#define UP     5

int getTaster() {
  int t = analogRead(0);
  static int old = NONE;
  int taster;
  if (t < 90) {
    taster = RIGHT;
  } else if (t < 240) {
    taster = UP;
  } else if (t < 350) {
    taster = DOWN;
  } else if (t < 600) {
    taster = LEFT;
  } else if (t < 1000) {
    taster = SELECT;
  }
  if (taster != old) {
    old = taster;
    return taster;
  } else {
    return NONE;
  }
}

struct koordinata {
  int x;
  int y;
};

struct zlatnik {
  int x;
  int y;
  int pokupljen;
};

struct koordinata mine[8];
struct zlatnik zlato[3];
struct koordinata baza;
struct koordinata player;
int zlatnici;

void printTablu() {
  lcd.clear();
  for (int i = 0; i < 8; i++) {
    lcd.setCursor(mine[i].x, mine[i].y);
    lcd.print("*");
  }
  for (int i = 0; i < 3; i++) {
    if (!zlato[i].pokupljen) {
      lcd.setCursor(zlato[i].x, zlato[i].y);
      lcd.print("@");
    }
  }
  lcd.setCursor(baza.x, baza.y);
  lcd.print("X");
  lcd.setCursor(player.x, player.y);
  lcd.print("K");
}

void gameOver(int razlog) {
  lcd.clear();
  lcd.setCursor(0, 0);
  if (razlog) {
    lcd.print("MINA");
  } else {
    lcd.print("POBEDIO");
  }
  int taster;
  while ((taster = getTaster()) != SELECT);
  setup();
}

void setup() {
  lcd.begin(16, 2);
  randomSeed(analogRead(0));
  for (int i = 0; i < 8; i++) {
    mine[i].x = 2*i + 1;
    mine[i].y = random() % 2;
  }

  for (int i = 0; i < 3; i++) {
    int ok = 0;
    while (!ok) {
      int x = random() % 15;
      int y = random() % 2;
      ok = 1;
      for (int i = 0; i < 8; i++) {
        if (mine[i].x == x && mine[i].y == y) {
          ok = 0;
        }
      }
      zlato[i].x = x;
      zlato[i].y = y;
      zlato[i].pokupljen = 0;
    }
  }

  int ok = 0;
  while (!ok) {
    int x = random() % 15;
    int y = random() % 2;
    ok = 1;
    for (int i = 0; i < 8; i++) {
      if (mine[i].x == x && mine[i].y == y) {
        ok = 0;
      }
    }
    for (int i = 0; i < 3; i++) {
      if (zlato[i].x == x && zlato[i].y == y) {
        ok = 0;
      }
    }
    baza.x = x;
    baza.y = y;
  }

  player.x = player.y = 0;
  zlatnici = 3;
}

void loop() {
  printTablu();
  int taster;
  while ((taster = getTaster()) == NONE || taster == SELECT);
  if (taster == UP) {
    if (player.y > 0) player.y--;
  } else if (taster == DOWN) {
    if (player.y < 1) player.y++;
  } else if (taster == LEFT) {
    if (player.x > 0) player.x--;
  } else if (taster == RIGHT) {
    if (player.x < 15) player.x++;
  }

  for (int i = 0; i < 8; i++) {
    if (mine[i].x == player.x && mine[i].y == player.y) {
      gameOver(1);
    }    
  }
  for (int i = 0; i < 3; i++) {
    if (zlato[i].x == player.x && zlato[i].y == player.y && !zlato[i].pokupljen) {
      zlato[i].pokupljen = 1;
      zlatnici--;
    }
  }

  if (baza.x == player.x && baza.y == player.y && zlatnici == 0) {
    gameOver(0);
  }
}
