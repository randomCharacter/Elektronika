# 1) Prekid tajmera
Napisati program koji ispisuje `ABCd` i `----` na 7-segmentnom displeju PLS7 ploce na svakih 0.5s

# 2) Prekid tastera
Napisati program koji menja cifre `1-6` na desnom delu displeja. Pritiskom na taster `DESNO` zaustavlja se, a pritiskom na taster `LEVO` nastavlja sa radom
