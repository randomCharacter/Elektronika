# 1) CAPTCHA
Napisati program koji vrsi proveru da li je korisnik ljudsko bice.
Na LCD displeju ispisati string duzine 5-8 karaktera koji se generise na slucajan nacin
i koji od karaktera moze da sadrzi velika slova ('A'..'Z'), mala slova ('a'..'z') i decimalne
cifre ('0'..'9'). Zatim se ocekuje od korisnika da preko serijskog porta unese string
koji je ocitan na displeju. Ukoliko je korisnik korektno uneo string, u donjem redu displeja
se ispisuje `"ECCE HOMO!"`, a u suprotnom se ispisuje `"BOT"`. Nova provera startuje pritiskom
na taster `SELECT`.

# 2) TRCECA REKLAMA
Napisati program koji omogucava unos teksta reklame koja treba da
se prikazuje na LCD displeju. Prilikom unosa teksta, kursor treba da bude ukljucen.
Pomeranje kursora duz displeja vrsi se pomocu tastera `LEFT` i `RIGHT`, a izbor karaktera
koji se prikazuje na trenutnoj poziciji pomocu tastera `UP` i `DOWN`. Nakon sto je unesen ceo
tekst reklame, pritiskom na taster `SELECT` kursor se iskljucuje i tekst reklame se pomera
(skroluje) po displeju s desna na levo.

# 3) RIZIKO
Napisati program koji simulira bacanje kockica u igri "Riziko".
U gornjem redu LCD displeja ispisuje se broj kockica koje baca prvi igrac
(napadac) i koji se podesava pritiskom na tastere `UP/DOWN`. Nakon izbora 
broja kockica, pritiskom tastera `SELECT` pocinje izvlacenje. Prilikom izvlacenja, 
na odgovarajucoj poziciji na displeju se smenjuju cifre 1-6 frekvencijom od 10Hz 
(tj. pauza izmedju dve uzastopne promene iznosi 100ms). Pritiskom na taster `SELECT`, 
zaustavlja se "obrtanje" kockice, na displeju ostaje vrednost koja je bila 
u trenutku pritiska tastera i prelazi se na izvlacenje sledeceg broja.
Za drugog igraca (koji se brani), ponavlja se isti proces ciji rezultat se
prikazuje u donjem redu displeja. Nakon izvlacenja za oba igraca, program
nakon ponovnog pritiska tastera `SELECT`, program automatski racuna rezultat bitke 
na sledeci nacin: kockice se za oba igraca sortiraju po velicini od vecih
ka manjim brojevima i posmatraju se parovi brojeva koje su izvukli razliciti igraci, 
pri cemu se neuparene kockice zanemaruju. Za svaki par kockica, igrac koji je izvukao
manji broj gubi jedan tenkic, a u slucaju neresenog ishoda pobedjuje igrac 
koji se brani (odnosno napadac gubi tenkic). Primer:

```
 ----------------                 ----------------
|Napad   3> 3 6 4|               |6 4 3 -> Gubi 1 |
|Odbrana 2> 4 5  |  -> SELECT -> |5 4   -> Gubi 1 |
 ----------------                 ----------------
 ```