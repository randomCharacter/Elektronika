# ZADATAK 1
### CAPTCHA
Napisati program koji vrsi proveru da li je korisnik ljudsko bice.
Na LCD displeju ispisati string duzine 5-8 karaktera koji se generise na slucajan nacin
i koji od karaktera moze da sadrzi velika slova ('A'..'Z'), mala slova ('a'..'z') i decimalne
cifre ('0'..'9'). Zatim se ocekuje od korisnika da preko serijskog porta unese string
koji je ocitan na displeju. Ukoliko je korisnik korektno uneo string, u donjem redu displeja
se ispisuje `"ECCE HOMO!"`, a u suprotnom se ispisuje `"BOT"`. Nova provera startuje pritiskom
na taster `SELECT`.