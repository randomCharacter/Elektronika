#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

char *letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

void setup() {
  // Inicijalizuje lcd
  lcd.begin(16, 2);
  // Inicijalizuje serial
  Serial.begin(9600);
  // Uzima novi seed
  randomSeed(analogRead(A1));
  // Ceka zbog seriala
  delay(1000);
}

void loop() {
  // Cisti ekran
  lcd.clear();
  
  // Postavlja kursor na pocetak ekrana
  lcd.setCursor(0, 0);
  
  // Generise se duzina stringa
  int len = random(5, 8);

  // Generise captchu
  char captcha[9];
  for (int i = 0; i < len; i++) {
    captcha[i] = letters[random(0, 62)];
  }
  captcha[len] = '\0';

  // Ispisuje captchu
  lcd.print(captcha);

  // Ucitava string
  while(!Serial.available());
  String string = Serial.readString();

  // Pomera kursor na donji deo
  lcd.setCursor(0, 1);
  if (string == captcha) {
    lcd.print("ECCE HOMO!");
  } else {
    lcd.print("BOT");
  }

  // Ceka dok se taster SELECT ne pritisne
  while(analogRead(0) / 100 != 6);
}

