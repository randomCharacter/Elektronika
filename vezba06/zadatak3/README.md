# 3)
### RIZIKO
Napisati program koji simulira bacanje kockica u igri "Riziko".
U gornjem redu LCD displeja ispisuje se broj kockica koje baca prvi igrac
(napadac) i koji se podesava pritiskom na tastere `UP/DOWN`. Nakon izbora 
broja kockica, pritiskom tastera `SELECT` pocinje izvlacenje. Prilikom izvlacenja, 
na odgovarajucoj poziciji na displeju se smenjuju cifre 1-6 frekvencijom od 10Hz 
(tj. pauza izmedju dve uzastopne promene iznosi 100ms). Pritiskom na taster `SELECT`, 
zaustavlja se "obrtanje" kockice, na displeju ostaje vrednost koja je bila 
u trenutku pritiska tastera i prelazi se na izvlacenje sledeceg broja.
Za drugog igraca (koji se brani), ponavlja se isti proces ciji rezultat se
prikazuje u donjem redu displeja. Nakon izvlacenja za oba igraca, program
nakon ponovnog pritiska tastera `SELECT`, program automatski racuna rezultat bitke 
na sledeci nacin: kockice se za oba igraca sortiraju po velicini od vecih
ka manjim brojevima i posmatraju se parovi brojeva koje su izvukli razliciti igraci, 
pri cemu se neuparene kockice zanemaruju. Za svaki par kockica, igrac koji je izvukao
manji broj gubi jedan tenkic, a u slucaju neresenog ishoda pobedjuje igrac 
koji se brani (odnosno napadac gubi tenkic). Primer:

```
 ----------------                 ----------------
|Napad   3> 3 6 4|               |6 4 3 -> Gubi 1 |
|Odbrana 2> 4 5  |  -> SELECT -> |5 4   -> Gubi 1 |
 ----------------                 ----------------
 ```