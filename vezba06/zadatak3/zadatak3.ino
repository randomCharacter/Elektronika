#include <LiquidCrystal.h>

#define LENGTH 16
#define COLUMNS 2

#define SELECT 650
#define LEFT  420
#define DOWN  280
#define UP    120
#define RIGHT 20

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

void setup() {
  // Inicijalizuje lcd
  lcd.begin(16, 2);
  // Postavlja pin 0 za citanje
  pinMode(0, OUTPUT);
  // Postavlja A1 za citanje kako bi izgenerisao random broj
  pinMode(A1, OUTPUT);
  // Postavljanje kursora
  lcd.setCursor(0,0);
  // Prikazuje kursor
  lcd.cursor();
  // Generisanje random brojeva
  randomSeed(analogRead(A1));
  // Ceka zbog seriala
  delay(1000);
}

void loop() {
  lcd.clear();
  lcd.cursor();
  // Indikator selektovanosti
  int selected = 0;
  // Broj kocica prvog i drugog
  int num1 = 1, num2 = 1;
  // Niz kockica
  int dice1[3] = {0}, dice2[3] = {0};
  // Broj izgubljenih tenkova
  int lost1 = 0, lost2 = 0;

  // Ucitavanje broja kockice napada
  lcd.print("Napad   ");
  selected = 0;
  while (!selected) {
    lcd.print(num1);
    lcd.setCursor(8, 0);
    if (analogRead(0) > RIGHT && analogRead(0) < UP) {
      if (num1 < 3) {
        num1++;
      }
    } else if (analogRead(0) < DOWN) {
      if (num1 > 1) {
        num1--;
      }
    } else if (analogRead(0) < SELECT) {
      selected = 1;
    }
    // Cekanje zbog drzanja dugmeta
    delay(150);
  }

  lcd.noCursor();
  lcd.setCursor(9, 0);
  lcd.print("> ");

  // Generisanje kockica napada
  for (int i = 0; i < num1; i++) {
    selected = false;
    while (!selected) {
      lcd.setCursor(11 + 2 * i, 0);
      int t = random() % 6 + 1;
      lcd.print(t);
      if (analogRead(0) > LEFT && analogRead(0) < SELECT) {
        selected = true;
        dice1[i] = t;
      }
      delay(150);
    }
  }

  // Ucitavanje broja kockice odbrane
  lcd.setCursor(0, 1); // prelazak u drugu liniju
  lcd.print("Odbrana ");
  lcd.cursor();
  selected = false;
  while (!selected) {
    lcd.print(num2);
    lcd.setCursor(8, 1);
    if (analogRead(0) > RIGHT && analogRead(0) < UP) {
      if (num2 < num1) {
        num2++;
      }
    } else if (analogRead(0) < DOWN) {
      if (num2 > 1) {
        num2--;
      }
    } else if (analogRead(0) < SELECT) {
      selected = 1;
    }
    // Cekanje zbog drzanja dugmeta
    delay(150);
  }

  lcd.noCursor();
  lcd.setCursor(9, 1);
  lcd.print("> ");

  // Generisanje kockica odbrane
  for (int i = 0; i < num2; i++) {
    selected = false;
    while (!selected) {
      lcd.setCursor(11 + 2 * i, 1);
      int t = random() % 6 + 1;
      lcd.print(t);
      if (analogRead(0) > LEFT && analogRead(0) < SELECT) {
        selected = true;
        dice2[i] = t;
      }
    }
    delay(150);
  }

  // Cekanje na SELECT za prelaz na sledecu fazu
  selected = false;
  while (!selected) {
    if (analogRead(0) > LEFT && analogRead(0) < SELECT) {
        selected = true;
      }
  }

  delay(150);

  // Sortiranje nizova
  for (int i = 0; i < num1; i++) {
    for (int j = i; j < num1; j++) {
      if (dice1[j] > dice1[i]) {
        int t = dice1[j];
        dice1[j] = dice1[i];
        dice1[i] = t;
      }
    }
  }

  for (int i = 0; i < num2; i++) {
    for (int j = i; j < num2; j++) {
      if (dice2[j] > dice2[i]) {
        int t = dice2[j];
        dice2[j] = dice2[i];
        dice2[i] = t;
      }
    }
  }

  // Ispis nizova
  lcd.clear();

  lcd.setCursor(0, 0);
  for (int i = 0; i < num1; i++) {
    lcd.print(dice1[i]);
    lcd.print(" ");
  }

  lcd.setCursor(0, 1);
  for (int i = 0; i < num2; i++) {
    lcd.print(dice2[i]);
    lcd.print(" ");
  }

  // Racunanje gubitaka
  int m = num1 < num2 ? num1 : num2;
  for (int i = 0; i < m; i++) {
    if (dice2[i] < dice1[i]) {
      lost2++;
    } else {
      lost1++;
    }
  }

  // Ispis gubitaka
  lcd.setCursor(6, 0);
  lcd.print("-> Gubi ");
  lcd.print(lost1);

  lcd.setCursor(6, 1);
  lcd.print("-> Gubi ");
  lcd.print(lost2);

  // Ceka SELECT da ponovo krene
  selected = false;
  while (!selected) {
    if (analogRead(0) > LEFT && analogRead(0) < SELECT) {
        selected = true;
      }
  }

  delay(150);
}

