# ZADATAK 2
### TRCECA REKLAMA
Napisati program koji omogucava unos teksta reklame koja treba da
se prikazuje na LCD displeju. Prilikom unosa teksta, kursor treba da bude ukljucen.
Pomeranje kursora duz displeja vrsi se pomocu tastera `LEFT` i `RIGHT`, a izbor karaktera
koji se prikazuje na trenutnoj poziciji pomocu tastera `UP` i `DOWN`. Nakon sto je unesen ceo
tekst reklame, pritiskom na taster `SELECT` kursor se iskljucuje i tekst reklame se pomera
(skroluje) po displeju s desna na levo.