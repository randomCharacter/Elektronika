#include <LiquidCrystal.h>

#define LENGTH 16
#define COLUMNS 2

#define SELECT 650
#define LEFT  420
#define DOWN  280
#define UP    120
#define RIGHT 20

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

String letters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

void setup() {
  // Inicijalizuje lcd
  lcd.begin(16, 2);
  // Postavlja pin 0 za citanje
  pinMode(0, OUTPUT);
  // Postavljanje kursora
  lcd.setCursor(0,0);
  // Prikazuje kursor
  lcd.cursor();
  // Ceka zbog seriala
  delay(1000);
}

void loop() {
  // Indeks karaktera na datoj poziciji u stringu letters
  static int text[LENGTH][COLUMNS];
  // Pozicija kursora
  static int posX = 0, posY = 0;
  // Postavlja kursor na nove pozicije
  lcd.setCursor(posX, posY);
  if (analogRead(0) < RIGHT) { // ako je pritisnuto "right"
    // Pomera kursor udesno
    if (++posX >= LENGTH) {
      posX %= LENGTH;
      if (++posY >= COLUMNS) {
        posY %= COLUMNS;
      }
    }
  } else if (analogRead(0) < UP) { // ako je pritisnuto "up"
    // Prelazak na sledeci karakter iz stringa letters
    if (++text[posX][posY] >= letters.length()) {
      text[posX][posY] %= letters.length();
    }
    lcd.print(letters[text[posX][posY]]);
  } else if (analogRead(0) < DOWN) { // ako je pritisnuto  "down"
    // Prelazak na prethodni karakter iz stringa letters
    if (--text[posX][posY] >= letters.length()) {
      text[posX][posY] = letters.length() - 1;
    }
    lcd.print(letters[text[posX][posY]]);
  } else if (analogRead(0) < LEFT) { // ako je pritisnuto "left"
    // Pomera kursor ulevo
    if (--posX < 0) {
      posX = LENGTH - 1;
      if (--posY < 0) {
        posY %= COLUMNS;
      }
    }
  } else if (analogRead(0) < SELECT) { // ako je pritisnuto "select"
    // Do kraja samo ovo radi
    while(1) {
      // Iskljucuje kursor
      lcd.noCursor();
      // Skroluje ceo tekst ulevo
      lcd.scrollDisplayLeft();
      delay(200);
    }
  }
  delay(150);
}

